import Header1 from './Components/Header/Header1'
// import Header2 from './Components/Header/Header2'
import './App.css';
import Search from './Components/Search/Search';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Flights from './Components/Flights/Flights'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact>
          <div className='App'>
            <Header1 />
            {/* <Header2 /> */}
            <Search />
          </div>
        </Route>
        <Route path="/search">
           <Flights />
        </Route>

      </Switch>
    </BrowserRouter>
  );
}

export default App;
