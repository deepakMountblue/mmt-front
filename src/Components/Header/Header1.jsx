import React, { Component } from 'react'
import './header.css'
export class Header1 extends Component {
    render() {
        return (
            <div className="container">
                <div >
                    <img src="https://imgak.mmtcdn.com/pwa_v3/pwa_hotel_assets/header/mmtLogoWhite.png" alt="" width="113px" height="36px" />
                </div>
                <div className="iconsHeader">
                    <div className="joinIcons" >
                        <span className="landingSprite myTripsIcon"></span>
                        <p>My Trips</p>
                    </div>
                    <div className="joinIcons" >
                        <span className="landingSprite supportIcon"></span>
                        <p>24*7 Support</p>
                    </div>
                    <div className="joinIcons" >
                        <span className="landingSprite myTripsIcon"></span>
                        <p>My Trips</p>
                    </div>
                    <div className="joinBtn" >

                        <span className="myIcon">
                            <span className="landingSprite myIconWhite"> &nbsp;</span>
                        </span>
                        <p style={{color:'white',lineHeight:"10px",marginLeft:"10px"}}>Login or Create Account</p>

                    </div>
                </div>


            </div>
        )
    }
}

export default Header1
