import React, { Component } from 'react'
import "./header2.css"
export class Header2 extends Component {
    render() {
        return (
            <div className="headerOuter">
                <div className="inner">
                    <div className="linkLogo">
                        <span className="allLogo flightLogo"></span>
                        <p>Flights</p>
                    </div>

                </div>
            </div>
        )
    }
}

export default Header2
