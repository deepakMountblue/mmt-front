import React, { Component } from 'react'
import './sidepanel.css'
export class SidePanel extends Component {
    state = {
        filters: [{
            name: "Air India",
            price: 4053,
            checked: false
        }, {
            name: "Morning Departure",
            price: 3852,
            checked: false
        }, {
            name: "IndiGo",
            price: 3852,
            checked: false
        }, {
            name: "Non Stop",
            price: 4053,
            checked: false
        }, {
            name: "Vistara",
            price: 4054,
            checked: false
        }, {
            name: "Late Departure",
            price: 4053,
            checked: false
        }],
        stops:[{
            name: "Non Stops",
            price: 4053,
            checked: false
        },{
            name: "1 Stop",
            price: 4053,
            checked: false
        },{
            name: "1+ Stop",
            price: 4053,
            checked: false
        }]
    }
    handlechange = (e,i) => {
        const val = this.state.filters
        val[i].checked = !val[i].checked
        this.setState({ filters: [...val] })
    }
    changeStops=(e,i)=>{
        const val = this.state.stops
        val[i].checked = !val[i].checked
        this.setState({ stops: [...val] })
    }
    render() {
        return (
            <div className="sideCont">

                <div className="sideWrapper">
                    <div className="sideOuter">
                        <p>Popular Filters</p>
                        {this.state.filters.map((ele, i) => {
                            return (<div style={{
                                display: "flex",
                                justifyContent: "space-between",
                                padding:'10px',
                                fontSize:'14px'
                            }}>
                                <div key={i} style={{display:"flex",alignItems:"center"}}>
                                    <input type="checkbox" checked={ele.checked} style={{
                                        width: '18px',
                                        height: '18px',
                                        cursor: 'pointer',
                                        border: '2px solid #9b9b9b',

                                    }} onChange={(e) => this.handlechange(e,i)} />
                                    <label>{ele.name}</label>
                                </div>
                                <span>&#8377;{ele.price}</span>
                            </div>)
                        })}
                    </div>

                    <div className="sideOuter">
                        <p>One Way Price</p>
                        <input type="range" min="1" max="100" style={{width:"100%"}}/>
                    </div>
                    <div className="sideOuter">
                        <p>Stops from Mumbai</p>
                        {this.state.stops.map((ele, i) => {
                            return (<div style={{
                                display: "flex",
                                justifyContent: "space-between",
                                padding:'10px',
                                fontSize:'14px'
                            }}>
                                <div key={i} style={{display:"flex",alignItems:"center"}}>
                                    <input type="checkbox" name checked={ele.checked} style={{
                                        width: '18px',
                                        height: '18px',
                                        cursor: 'pointer',
                                        border: '2px solid #9b9b9b',

                                    }} onChange={(e) => this.changeStops(e,i)} />
                                    <label>{ele.name}</label>
                                </div>
                                <span>&#8377;{ele.price}</span>
                            </div>)
                        })}
                    </div>
                </div>

            </div>
        )
    }
}

export default SidePanel
