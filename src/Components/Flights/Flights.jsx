import React, { Component } from 'react'
import SidePanel from './SidePanel'
import './flight.css'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
export class Flights extends Component {

    state={
        flights:[
            {
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4253,
                start:"08:55",
                end:"15:25",
                duration:"6 h 30 m"
            },
            {
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"11:45",
                end:"17:30",
                duration:"5 h 15 m"
            },{
                name:"Air India",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/AI.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"09:30",
                end:"11:10",
                duration:"01 h 40 m"
            },{
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4253,
                start:"08:55",
                end:"15:25",
                duration:"6 h 30 m"
            },
            {
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"11:45",
                end:"17:30",
                duration:"5 h 15 m"
            },{
                name:"Air India",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/AI.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"09:30",
                end:"11:10",
                duration:"01 h 40 m"
            },{
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4253,
                start:"08:55",
                end:"15:25",
                duration:"6 h 30 m"
            },
            {
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"11:45",
                end:"17:30",
                duration:"5 h 15 m"
            },{
                name:"Air India",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/AI.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"09:30",
                end:"11:10",
                duration:"01 h 40 m"
            },{
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4253,
                start:"08:55",
                end:"15:25",
                duration:"6 h 30 m"
            },
            {
                name:"Indigo",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"11:45",
                end:"17:30",
                duration:"5 h 15 m"
            },{
                name:"Air India",
                logo:"https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/AI.png?v=6",
                source:"banglore",
                dest:"mumbai",
                price:4600,
                start:"09:30",
                end:"11:10",
                duration:"01 h 40 m"
            }
        ]
    }

    render() {
        return (
            <div className="allComp">
                <span className="bg"></span>
                <div className="flightCont">
                    <SidePanel />
                    <div className="flightDetails">
                        <p>Flights from Mumbai to Bengaluru</p>

                        <div className="mainFlightDetail">

                            <div className="weekFare">
                                <div className="fareIcon">
                                    <ArrowBackIosIcon />
                                </div>
                                <div className="fareItem">
                                    <p>Thu, May 27</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem">
                                    <p>Thu, May 28</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem">
                                    <p>Thu, May 29</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem">
                                    <p>Thu, May 30</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem">
                                    <p>Thu, May 31</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem">
                                    <p>Thu, Jun 1</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem">
                                    <p>Thu, Jun 2</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareItem" >
                                    <p>Thu, Jun 3</p>
                                    <span>&#8377;3825</span>
                                </div>
                                <div className="fareIcon" >
                                    <ArrowForwardIosIcon />
                                </div>
                            </div>
                            <div className="sortFlight">
                                <div >
                                    <span style={{ fontWeight: "900" }}>Sorted By:</span>
                                    <div className="useless"></div>
                                    <span className="useful" style={{ width: "100px" }}>Departure</span>
                                    <span className="useful" style={{ width: "130px", marginLeft: "40px" }}>Duration</span>
                                    <span className="useful" style={{ width: "80px" }}>Arrival</span>
                                    <span className="useful" style={{ fontWeight: "900", width: "140px", textAlign: 'right' }}>Price</span>
                                </div>

                            </div>

                            
                            {this.state.flights.map((ele,i)=>{
                                return(<div key={i} className="listcard">
                                <div style={{
                                    display:"flex",
                                    alignItems:"center",
                                    width:"142px"
                                }}>
                                    <span className="flightLogo" style={{backgroundImage:`url(${ele.logo})`}}></span>
                                    <p style={{fontSize:"14px",margin:"0 8px"}}>{ele.name}</p>
                                </div>
                                <div style={{
                                    width:"400px"
                                }}>
                                    <div style={{
                                        width: '100%',
                                        border: '1px solid transparent',
                                        padding: '5px 8px 5px 80px',
                                        display:"flex",
                                        fontSize:'12px'
                                    }}>
                                        <div style={{width:"100px",display:"flex",flexDirection:"column",justifyContent:"center",}}>
                                            <p style={{fontWeight:"900",fontSize:"18px",margin:"0 "}}>{ele.start}</p>
                                            <span style={{color:"#4a4a4a"}}>{ele.source}</span>
                                        </div>
                                        <div style={{display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"center",padding:"0 4px"}}>
                                            <span style={{fontSize:'12px'}}>{ele.duration}</span>
                                            <span>1 stop via banglore</span>
                                        </div>
                                        <div style={{width:"100px",display:"flex",flexDirection:"column",justifyContent:"center",alignItems:"flex-end"}}>
                                            <p style={{fontWeight:"900",fontSize:"18px",margin:"0 "}}>{ele.end}</p>
                                            <span style={{color:"#4a4a4a"}}>{ele.dest}</span>
                                        </div>
                                    </div>
                                </div>
                                    <div style={{width:"185px",display:"flex",justifyContent:"flex-end",alignItems:"center"}}>
                                        <p style={{fontSize:"20px",margin:0}}>{ele.price}</p>
                                    </div>

                            </div>)
                            })}

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default Flights
