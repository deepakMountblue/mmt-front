import React, { Component } from 'react'
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import SearchIcon from '@material-ui/icons/Search';
import {Link} from 'react-router-dom'
import './search.css'
export class Search extends Component {
    state = {
        types: ['ONEWAY', 'ROUND TRIP', 'MULTI CITY'],
        wayCheck: "ONEWAY",
        clicked: "",
        source: "Delhi",
        cities: ['Delhi', 'Mumbai', 'Banglore','Pune'],
        dest: "Banglore",
        fareType: ["Regular Fare", "student fare", "double seat", "senior citizen", "armed forces"],
        selectedFare: 0,
        filterSearch:['Delhi', 'Mumbai', 'Banglore','Pune']
    }

    filterSearch=(e)=>{
        const filtered=this.state.cities.filter(ele=>ele.toLowerCase().includes(e.target.value.toLowerCase()))
        this.setState({filterSearch:filtered})
    }
    render() {
        return (
            <div className="searchBox">
                <div className="searchMain">

                    <div className="flighType" >
                        {this.state.types.map((ele, i) => {
                            if (ele !== this.state.wayCheck) {
                                return (<div key={i} className="wayBtn" onClick={() => this.setState({ wayCheck: ele })}>
                                    <RadioButtonUncheckedIcon style={{ color: "#9b9b9b", marginRight: "5px", fontSize: "18px" }} />
                                    <p style={{
                                        color: "#9b9b9b"
                                    }}>{ele}</p>
                                </div>)
                            }
                            else {
                                return (<div key={i} className="wayBtn2">
                                    <CheckCircleIcon style={{ color: "black", marginRight: "5px", fontSize: "15px" }} />
                                    <p >{ele}</p>
                                </div>)
                            }
                        })}
                    </div>

                    <div className="searchBoxes">
                        <div className="searchInputBox">
                            <h3 style={{ color: "#9b9b9b" }}>From</h3>
                            <div className="changeInput" >
                                {this.state.clicked === "source" ? <div className="inputOverflow">

                                    <div style={{ position: 'relative' }}>
                                        <SearchIcon style={{ position: "absolute", top: '10px', left: '5px' }} />
                                        <input type="text" className="hiddenInput" onBlur={() => {
                                            this.setState({ clicked: "" })
                                        }} autoFocus placeholder="From" onChange={this.filterSearch}/>
                                    </div>

                                    <div className="hiddenOptions" style={{
                                        fontWeight: '400',
                                        fontSize: "14px"
                                    }}>
                                        {this.state.filterSearch.map((ele, i) => {
                                            return (<div className="hiddenList" key={ele} onMouseDown={() => {
                                                this.setState({ source: ele, clicked: "" })
                                            }}><p>{ele}</p></div>)
                                        })}

                                    </div>

                                </div> : <h1 onClick={() => this.setState({ clicked: "source" })}>{this.state.source}</h1>}
                            </div>
                            <p>DEL,Delhi Airport India</p>
                        </div>

                        <div className="searchInputBox" style={{ borderRadius: "0" }}>
                            <h3 style={{ color: "#9b9b9b" }}>To</h3>

                            <div className="changeInput" >
                                {this.state.clicked === "dest" ? <div className="inputOverflow">

                                    <div style={{ position: 'relative' }}>
                                        <SearchIcon style={{ position: "absolute", top: '10px', left: '5px' }} />
                                        <input type="text" className="hiddenInput" onBlur={() => {
                                            this.setState({ clicked: "" })
                                        }} autoFocus onChange={this.filterSearch} />
                                    </div>
                                    <div className="hiddenOptions" style={{
                                        fontWeight: '400',
                                        fontSize: "14px"
                                    }}>
                                        {this.state.filterSearch.map((ele, i) => {
                                            return (<div className="hiddenList" key={ele} onMouseDown={() => {
                                                this.setState({ dest: ele, clicked: "" })
                                            }}>{ele}</div>)
                                        })}

                                    </div>

                                </div> : <h1 onClick={() => this.setState({ clicked: "dest" })}>{this.state.dest}</h1>}
                            </div>
                            <p>DEL,Delhi Airport India</p>
                        </div>

                        <div className="departure">
                            <span>Departure</span>
                            <h1>27 <span style={{ fontSize: "20px", fontWeight: "400" }}>May'21</span></h1>
                            <p>Thursday</p>
                        </div>
                        <div className="departure">
                            <span>Return</span>
                            <h1>27 <span style={{ fontSize: "20px", fontWeight: "400" }}>May'21</span></h1>
                            <p>Thursday</p>
                        </div>
                        <div className="departure">
                            <span>TRAVELLER {'&'} CLASS</span>
                            <h1>27 <span style={{ fontSize: "20px", fontWeight: "400" }}>May'21</span></h1>
                            <p>Thursday</p>
                        </div>
                    </div>

                    <div className="fareTypes">
                        {this.state.fareType.map((ele, i) => {
                            if (i === this.state.selectedFare) {
                                return (<button key={i} className="fareBtn" style={{
                                    background: '#008cff',
                                    color: '#fff'
                                }}>
                                    {ele}
                                </button>)
                            }
                            else {
                                return (<button key={i} onClick={() => this.setState({ selectedFare: i })} className="fareBtn">
                                    {ele}
                                </button>)
                            }
                        })}
                    </div>

                    <div style={{
                        position: "relative",
                        display:"flex",
                        width:'100%',
                        justifyContent:"center",
                        top:60
                    }}>
                       <Link to='/search'> <button className="searchBtnMain">Search</button></Link>
                    </div>
                 

                </div>

            </div>
        )
    }
}

export default Search
